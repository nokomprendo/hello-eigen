with import<nixpkgs> {};
stdenv.mkDerivation {
    name = "helloeigen";
    src = ./.;
    buildInputs = [ cmake eigen3_3 pkgconfig ];
}

