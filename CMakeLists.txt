cmake_minimum_required( VERSION 3.0 )
project( helloeigen )

find_package( PkgConfig REQUIRED )
pkg_check_modules( MYPKG REQUIRED eigen3 )
include_directories( ${MYPKG_INCLUDE_DIRS} )

add_executable( helloeigen helloeigen.cpp )
install( TARGETS helloeigen DESTINATION bin )

